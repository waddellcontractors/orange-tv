package tv.orange.models.abc

interface Feature {
    fun onDestroyFeature()
    fun onCreateFeature()
}