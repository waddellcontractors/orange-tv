package tv.orange.models.abc

enum class BadgePackageSet(val value: String) {
    Ffz("ffz"),
    Stv("stv"),
    Chatterino("cha"),
    Homies("chi"),
    TwitchMod("tm")
}